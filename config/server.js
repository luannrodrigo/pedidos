// Importando os modulos
const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const expressSession = require('express-session');

const app = express();

//setando a engine de views
app.set('view engine', 'ejs')
//setando o diretorio de views
app.set('views', './app/views')

app.use(express.static('./app/public'));

// app.use(express.static('./app/public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(expressValidator());


consign()
	.include('app/routes')
	.then('config/dbConnection.js')
	.then('app/models')
	.then('app/controllers')
	.into(app)

  module.exports = app;
