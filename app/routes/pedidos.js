module.exports = function(application) {
    application.get('/pedidos', function(req, res) {
        application.app.controllers.pedidos.mostraPedidos(application, req, res);
    });
    application.put('/NovoPedido', function(req, res){
        application.app.controllers.pedidos.NovoPedido(application, req, res);
    });
};
